#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size, choice, input;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	while (true)
	{
	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";

	cout << "\n\n[1] Remove Element at Index" << endl;
	cout << "[2] Search Element" << endl;
	cout << "[3] Expand and Generate Random Values" << endl;
	cin >> choice;

	
		if (choice == 1)
		{
			cout << "\n\nEnter index to remove: ";
			cin >> input;
			unordered.remove(input);
			ordered.remove(input);
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			system("pause");
			system("cls");
		}
		else if (choice == 2)
		{
			cout << "\n\nEnter number to search: ";
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			system("pause");
			system("cls");
		}
		else if (choice == 3)
		{
			cout << "Input size of expansion: ";
			cin >> input;

			for (int i = 0; i < input; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "Arrays have been expanded: " << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			system("pause");
			system("cls");
			
		}
	}
	
}